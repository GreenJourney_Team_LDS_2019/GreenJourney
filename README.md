**Green Journey**

As mudanças climáticas têm tido um papel importante no desenvolvimento de novas tecnologias ecológicas, tais como os veículos eléctricos.
Estes são importantes, no sentido de ajudar na conservação de recursos e diminuição das emissões de dióxido de carbono para a atmosfera.
Apesar da evolução bastante acentuada nestes últimos anos, uma das grandes falhas desta tecnologia é a falta de autonomia e de postos de carregamento.

**Getting Started**

*A definir...*

**Installing**

*A definir...*

**Deployment**


**Built With**

*em atualização*

     Visual Paradigm - Diagrams
     Microsoft Word - SRS Document
     Balsamiq Mockups 3 - Mockups 
    

**Team**

*  Luís Marques
*  Hugo Napoleão
*  Vasco Neves
*  Miguel Carvalho
*  Pedro Cardoso
*  Luís Teixeira



