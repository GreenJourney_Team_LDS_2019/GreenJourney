import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IChargingStation } from './icharging-station';

@Injectable({
  providedIn: 'root'
})
export class ChargingStationService {
  private _URL: string = "https://localhost:44307/api/POIs";
  constructor(private http: HttpClient) { }

  getChargingStationFromAPI(): Observable<IChargingStation[]> {
    return this.http.get<IChargingStation[]>(this._URL);
  }

  getChargingStationbyId(id): Observable<IChargingStation[]> {
    return this.http.get<IChargingStation[]>(this._URL + "/id/" + id);
  }

  newChargingStation(station: IChargingStation): Observable<IChargingStation> {
    return this.http.post<any>(this._URL, station);
  }

  deleteChargingStation(id:string) {
    return this.http.delete<any>(this._URL + id);
  }

  updateChargingStation(url, station:IChargingStation) {
    return this.http.patch<any>(url, station);
  }
}
