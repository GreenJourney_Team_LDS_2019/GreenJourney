export class IChargingStation {
    public id: any;
    public name: string;
    public description: string;
    public locationName: string;
    public latitude: any;
    public longitude: any;
}
