import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';




@Injectable({
  providedIn: 'root'
})
export class MarkerService {

  points: string = '/src/app/rodents.geojson';


  constructor(private http: HttpClient) {

  }


  makePoints(map: L.Map): void {

    this.http.get(this.points).subscribe((res: any) => {
      for (const p of res.features) {
        const lat = p.geometry.coordinates[0];
        const lon = p.geometry.coordinates[1];
        //const marker = L.marker([lon, lat]).addTo(map);
      }
    })

  }
}
