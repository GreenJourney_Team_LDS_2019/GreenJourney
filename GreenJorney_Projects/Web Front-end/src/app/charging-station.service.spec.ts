import { TestBed } from '@angular/core/testing';

import { ChargingStationService } from './charging-station.service';

describe('ChargingStationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChargingStationService = TestBed.get(ChargingStationService);
    expect(service).toBeTruthy();
  });
});
