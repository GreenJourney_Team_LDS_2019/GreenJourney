import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';
import 'leaflet-routing-machine';
import 'leaflet-control-geocoder';
import { GeoSearchControl, OpenStreetMapProvider } from 'leaflet-geosearch';
import { MarkerService } from '../_services/marker.service';
import 'leaflet-openweathermap';
import 'leaflet-easybutton';
import { ChargingStationService } from '../charging-station.service';
import { PoiService } from '../poi.service';
import 'esri-leaflet';
import { PontosVerdes } from '../models/espacos-verdes';

@Component({
	selector: 'app-map',
	templateUrl: './map.component.html',
	styleUrls: ['./map.component.css'],

})

export class MapComponent implements OnInit {
	
	requestedPoints: PontosVerdes[];
	constructor(private myHttpService: PoiService) { }

	ngOnInit(): void {
		this.mapOnInit();
		//this.getPoints();
	}

	getPointsFromObject() {
		var points = this.myHttpService.getData();
		for (let i = 0; i < points.length; i++) {
			console.log(points[i].name);
		}
		return points;
	}

	/*getPoints() {
		var points = this.myHttpService.getDataFromAPI().subscrive(data => {
			this.requestedPoints = data;
		});
		for (let i = 0; i < points.length; i++) {
			console.log(points[i].name);
		}
		return points;
	}*/

	mapOnInit() {
		var groupPoints = this.getPointsFromObject();
		navigator.geolocation.getCurrentPosition(function (location) {

			var latlng = new L.LatLng(location.coords.latitude, location.coords.longitude);

			//Sets the map view
			var map = L.map('map', {
				center: latlng,
				zoom: 16,
				zoomControl: false
			});

			//Gets an icon for the current location marker
			var myIcon = L.icon({
				iconUrl: './assets/currentLocation.jpg',
				iconSize: [15, 15]
			});

			//Adds a marker in the map with the current location
			var currentLocation = L.marker(latlng, { icon: myIcon }).addTo(map);

			//Adds a marker accuracy for the current location in the map
			var currentAccuracy = L.circle(latlng, {
				color: '#0330fc',
				fillColor: '#7891ff',
				fillOpacity: 0.5,
				radius: (location.coords.accuracy) / 4
			}).addTo(map);

			//Sets the map background to street view
			var normal = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
				attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
				maxZoom: 19,
				id: 'mapbox/streets-v11',
				accessToken: 'pk.eyJ1IjoiZG9nZXJtYW4yMDIiLCJhIjoiY2szb2poM295MGx4aDNkcWV3ejA2ZmlndyJ9.GpmCzw16Ma5gKPBXndqCEg'
			}).addTo(map);

			//Sets the map background to satellite view
			var satelite = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
				attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
				maxZoom: 19,
				id: 'mapbox/satellite-v9',
				accessToken: 'pk.eyJ1IjoiZG9nZXJtYW4yMDIiLCJhIjoiY2szb2poM295MGx4aDNkcWV3ejA2ZmlndyJ9.GpmCzw16Ma5gKPBXndqCEg'
			}).addTo(map);

			//Routing control
			L.Routing.control({
				waypoints: [
					latlng,
					latlng
				],
				routeWhileDragging: true
			}).addTo(map);

			var zoom = L.control.zoom({
				position: 'bottomright'
			});

			var menuControl = L.Control.extend({
				options: {
					position: 'topleft'
				},

				onAdd: function (map) {
					//var ifExists = false;

					var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-menu-custom');

					container.style.backgroundColor = 'white';
					//container.style.border = "1px solid black";
					container.style.backgroundImage = 'url(./assets/search_box_hamburguer.png)';
					container.style.backgroundSize = "45px 45px";
					container.style.width = '45px';
					container.style.height = '45px';

					container.onclick = function () {
						var menu = document.getElementById("visible-menu");
						var smkScreen = document.getElementById("smoke-screen");
						smkScreen.style.visibility = "visible";
						menu.style.visibility = "visible";
					}
					return container;
				}
			});

			var xx = {
				'Satélite': satelite,
				'Mapa': normal
			}

			L.control.layers(xx, {}, {
				collapsed: false,
				position: 'bottomleft'
			}).addTo(map);

			var centerControl = L.Control.extend({
				options: {
					position: 'bottomright'
				},

				onAdd: function (map) {
					var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-center-custom');

					container.style.backgroundColor = 'white';
					container.style.backgroundImage = 'url(./assets/currentLocation.jpg)';
					container.style.backgroundSize = "26px 26px";
					container.style.width = '26px';
					container.style.height = '26px';

					container.onclick = function () {
						map.setView(latlng, 16);
					}
					return container;
				}
			});

			//Adds a search bar
			var provider = new OpenStreetMapProvider();
			var searchControl = new GeoSearchControl({
				provider: provider,
				showMarker: true,
				showPopup: true,

				marker: {
					icon: new L.Icon.Default(),
					draggable: false,
				},
				popupFormat: ({ query, result }) => result.label,
				maxMarkers: 1,
				retainZoomLevel: false,
				animateZoom: true,
				autoClose: true,
				searchLabel: 'Enter address',
				keepResult: true
			});
			map.addControl(searchControl);

			zoom.addTo(map);
			map.addControl(new menuControl());
			map.addControl(searchControl);
			map.addControl(new centerControl());

			var simpleIcon = L.icon({
				iconUrl: './assets/currentLocation.jpg',
				iconSize: [15, 15]
			});

			for (let i = 0; i < groupPoints.length; i++) {
				//console.log(groupPoints[i].name);
				var customPopup = '<h3>Name: </h3>' + groupPoints[i].name + '<br><h3>Address:</h3>' + groupPoints[i].address;

				var customPopupOptions = {
		        	'maxWidth': 500,
	        	};

				L.marker([groupPoints[i].latitude, groupPoints[i].longitude]).addTo(map).bindPopup(customPopup, customPopupOptions);
			}
		});
	}
}