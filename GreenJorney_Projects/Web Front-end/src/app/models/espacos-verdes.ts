export class PontosVerdes {
	id: number;
	name: string;
	description: string;
	lat: number;
	long: number;
	img: string;
}