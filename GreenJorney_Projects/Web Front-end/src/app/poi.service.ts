import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { PontosVerdes } from './models/espacos-verdes';
import { retry, catchError } from 'rxjs/operators';

@Injectable()
export class PoiService {

	_URL: string = "https://localhost:5001/api/ChargingStations";
	points: PontosVerdes;

	constructor(private http: HttpClient) { }

	/*getDataFromAPI(): Observable<PontosVerdes[]> {
		return this.http.get<PontosVerdes[]>(this._URL)
			.pipe(
				retry(2),
				catchError(this.handleError))
	}

	handleError(error: HttpErrorResponse) {
		let errorMessage = '';
		if (error.error instanceof ErrorEvent) {
			// Erro ocorreu no lado do client
			errorMessage = error.error.message;
		} else {
			// Erro ocorreu no lado do servidor
			errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
		}
		console.log(errorMessage);
		return throwError(errorMessage);
	};*/

	getData() {
		return [{
		id:2,name:"Posto Av. de França",address:"Av. da França 315-213, Porto",latitude:41.16172,longitude:-8.62794,chargerType:"DC & AC"
		},
		{
		id:3,name:"Posto Praça do Dr. Francisco Sá Carneiro",address:"Praça do Dr. Francisco Sá Carneiro 27, 4350-042 Porto",latitude:41.16196,longitude:-8.59235,chargerType:"DC"
		},
		{
		id:4,name:"Posto Av. Sidónio Pais",address:"Av. Sidnio Pais n 415 (Rotunda do Bessa), 4100-012 Porto",latitude:41.164407,longitude:-8.639212,chargerType:"DC"
		},
		{
		id:5,name:"Posto Área de serviço Q8",address:"Estrada da Circunvalação 10853, 4250-152 Porto",latitude:41.18025,longitude:-8.63494,chargerType:"DC & AC"
		},
		{
		id:6,name:"Posto Campo 24 de Agosto ",address:"Jardim do Campo 24 de Agosto, 4300-096 Porto",latitude:41.149764,longitude:-8.598908,chargerType:"DC"
		},
		{
		id:7,name:"Posto Zona industrial ",address:"Estrada da Circunvalação 10853, 4250-152 Porto",latitude:41.18025,longitude:-8.63494,chargerType:"DC"}]
	}
}