﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GreenJourney.Domain.Models;
using GreenJourney.Domain.Services;
namespace GreenJourney.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChargingStationsController : ControllerBase
    {
        private readonly IChargingStationService _chargingStationService;


        public ChargingStationsController(IChargingStationService chargingStationService)
        {
            _chargingStationService = chargingStationService;
        }

        //GET:  api/ChargingStations
        [HttpGet]
        public async Task<IEnumerable<ChargingStation>> GetChargingStationsList()
        {
            var station = await _chargingStationService.ListAsync();

            return station;
        }

        //GET: api/ChargingStations/Id
        [HttpGet("{id}")]
        public async Task<ActionResult<ChargingStation>> GetChargingStation(int id) 
        {
            var station = await _chargingStationService.FindByIdAsync(id);

            if (station == null) 
            {
                return NotFound();
            }

            return station;
        }

        //POST: api/ChargingStations
        [HttpPost]
        public ActionResult<ChargingStation> PostChargingStationAsync([FromBody] ChargingStation station)
        {
            if (!ModelState.IsValid) 
            {
                return BadRequest();
            }

            _chargingStationService.Save(station);
            
            return Ok(station);
        }

        //PUT   api/ChargingStations/Id
        [HttpPut("{Id}")]
        public ActionResult<ChargingStation> PutPOI(int id, ChargingStation station)
        {
            var modified = _chargingStationService.Update(id, station);
            if (modified == null)
            {
                return BadRequest();
            }
            else
            {
                return modified;
            }
        }

        //DELETE   api/ChargingStations/Id
        [HttpDelete("{Id}")]
        public ActionResult<ChargingStation> DeletePOI(int id)
        {
            var deleted = _chargingStationService.Delete(id);

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            else if (deleted == null)
            {
                return NotFound();
            }
            else
            {
                return deleted;
            }
        }
    }
}


