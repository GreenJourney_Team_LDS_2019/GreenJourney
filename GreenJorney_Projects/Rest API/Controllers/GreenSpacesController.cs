﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GreenJourney.Domain.Models;
using GreenJourney.Domain.Services;
namespace GreenJourney.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GreenSpacesController : ControllerBase
    {
        private readonly IGreenSpaceService _greenSpaceService;


        public GreenSpacesController(IGreenSpaceService greenSpaceService)
        {
            _greenSpaceService = greenSpaceService;
        }

        //GET:  api/GreenSpaces
        [HttpGet]
        public async Task<IEnumerable<GreenSpace>> GetGreenSpacesList()
        {
            var space = await _greenSpaceService.ListAsync();

            return space;
        }

        //GET: api/GreenSpaces/Id
        [HttpGet("{id}")]
        public async Task<ActionResult<GreenSpace>> GetGreenSpace(int id) 
        {
            var space = await _greenSpaceService.FindByIdAsync(id);

            if (space == null) 
            {
                return NotFound();
            }

            return space;
        }

        //POST: api/GreenSpaces
        [HttpPost]
        public ActionResult<GreenSpace> PostChargingStationAsync([FromBody] GreenSpace space)
        {
            if (!ModelState.IsValid) 
            {
                return BadRequest();
            }

            _greenSpaceService.Save(space);
            
            return Ok(space);
        }

        //PUT   api/GreenSpaces/Id
        [HttpPut("{Id}")]
        public ActionResult<GreenSpace> PutPOI(int id, GreenSpace space)
        {
            var modified = _greenSpaceService.Update(id, space);
            if (modified == null)
            {
                return BadRequest();
            }
            else
            {
                return modified;
            }
        }

        //DELETE   api/ChargingStations/Id
        [HttpDelete("{Id}")]
        public ActionResult<GreenSpace> DeletePOI(int id)
        {
            var deleted = _greenSpaceService.Delete(id);

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            else if (deleted == null)
            {
                return NotFound();
            }
            else
            {
                return deleted;
            }
        }
    }
}


