﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using GreenJourney.Domain.Models;

namespace GreenJourney.DataAcess
{
    public class AppDBContext : DbContext
    {
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {

        }

        public DbSet<ChargingStation> ChargingStations { get; set; }
        public DbSet<GreenSpace> GreenSpaces { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ChargingStation>().ToTable("Charging Stations");
            builder.Entity<ChargingStation>().HasKey(p => p.Id);
            builder.Entity<ChargingStation>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<ChargingStation>().Property(p => p.Name).IsRequired().HasMaxLength(200);
            builder.Entity<ChargingStation>().Property(p => p.Address).IsRequired().HasMaxLength(1000);
            builder.Entity<ChargingStation>().Property(p => p.Latitude).IsRequired();
            builder.Entity<ChargingStation>().Property(p => p.Longitude).IsRequired();
            builder.Entity<ChargingStation>().Property(p => p.ChargerType).IsRequired();

            builder.Entity<ChargingStation>().HasData
            (
                new ChargingStation { Id = 1, Name = "Posto Rua de Anselmo Braancamp", Address = "R. de Anselmo Braancamp 385-245, 4000-487 Porto", Latitude = 41.154052, Longitude = -8.600702, ChargerType = "DC & AC"},
                new ChargingStation { Id = 2, Name = "Posto Av. de França", Address = "Av. da França 315-213, Porto", Latitude = 41.16172, Longitude = -8.62794, ChargerType = "DC & AC"},
                new ChargingStation { Id = 3, Name = "Posto Praça do Dr. Francisco Sá Carneiro", Address = "Praça do Dr. Francisco Sá Carneiro 27, 4350-042 Porto", Latitude = 41.16196, Longitude = -8.59235, ChargerType = "DC"},
                new ChargingStation { Id = 4, Name = "Posto Av. Sidónio Pais", Address = "Av. Sidnio Pais n 415 (Rotunda do Bessa), 4100-012 Porto", Latitude = 41.164407, Longitude = -8.639212, ChargerType = "DC" },
                new ChargingStation { Id = 5, Name = "Posto Área de serviço Q8", Address = "Estrada da Circunvalação 10853, 4250-152 Porto", Latitude = 41.18025, Longitude = -8.63494, ChargerType = "DC & AC" },
                new ChargingStation { Id = 6, Name = "Posto Campo 24 de Agosto ", Address = "Jardim do Campo 24 de Agosto, 4300-096 Porto", Latitude = 41.149764, Longitude = -8.598908, ChargerType = "DC" },
                new ChargingStation { Id = 7, Name = "Posto Zona industrial ", Address = "Estrada da Circunvalação 10853, 4250-152 Porto", Latitude = 41.18025, Longitude = -8.63494, ChargerType = "DC" }
            );

            builder.Entity<GreenSpace>().ToTable("Green Spaces");
            builder.Entity<GreenSpace>().HasKey(p => p.Id);
            builder.Entity<GreenSpace>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<GreenSpace>().Property(p => p.Name).IsRequired().HasMaxLength(200);
            builder.Entity<GreenSpace>().Property(p => p.Description).IsRequired().HasMaxLength(600);
            builder.Entity<GreenSpace>().Property(p => p.Address).IsRequired().HasMaxLength(1000);
            builder.Entity<GreenSpace>().Property(p => p.Latitude).IsRequired();
            builder.Entity<GreenSpace>().Property(p => p.Longitude).IsRequired();
            builder.Entity<GreenSpace>().Property(p => p.Image).IsRequired();

            builder.Entity<GreenSpace>().HasData
          (
              new GreenSpace { Id = 1, Name = "Centro de Educação Ambiental das Ribeiras de Gaia", Description = "Instituição publica de educação ambiental, dependente da Águas de Gaia, EM, que se dedica a educação ambiental.", Address = "Av. Fernão Magalhães 478", Latitude = 41.065229, Longitude = -8.653473, Image = "http://www.portoenorte.pt/fotos/oquefazer/1459423184.9344_3751746245a620a1125115.jpg" },
              new GreenSpace { Id = 2, Name = "Jardim do Morro", Description = "Espaço verde na freguesia de Santa Marinha, em Vila Nova de Gaia, Portugal.", Address = "4430-188 Vila Nova de Gaia", Latitude = 41.137627, Longitude = -8.608684, Image = "https://2.bp.blogspot.com/-XVEZBZ4N_30/Wbrn7kw1_EI/AAAAAAABFIc/QowrcKLiomg1Pcyk9f66FlIrXeTn6UJFQCLcBGAs/s1600/jardim%2Bdo%2Bmorro%2B11.jpg" },
              new GreenSpace { Id = 3, Name = "Jardim de Soares dos Reis", Description = " Espaço verde localizado na freguesia de Mafamude, em Vila Nova de Gaia, Portugal.", Address = "4430-188 Vila Nova de Gaia", Latitude = 41.137627, Longitude = -8.608684, Image = "https://2.bp.blogspot.com/-XVEZBZ4N_30/Wbrn7kw1_EI/AAAAAAABFIc/QowrcKLiomg1Pcyk9f66FlIrXeTn6UJFQCLcBGAs/s1600/jardim%2Bdo%2Bmorro%2B11.jpg" },
              new GreenSpace { Id = 4, Name = "Quinta do Chantre", Description = "Espaço verde localizado na freguesia de Leça do Balio, concelho de Matosinhos, distrito do Porto, em Portugal, numa zona de fronteira com o concelho da Maia.", Address = "4430-315 Vila Nova de Gaia", Latitude = 41.122906, Longitude = -8.612096, Image = "http://www.patrimoniocultural.gov.pt/static/data/cache/57/74/577405c8a719a7677ca8dd8368030b6a.jpg" },
              new GreenSpace { Id = 5, Name = "Parque Biológico de Gaia", Description = "Reserva protegida situada no vale do Rio Febros, nas freguesias de Avintes e Vilar de Andorinho.", Address = "Leça do Balio", Latitude = 41.221313, Longitude = -8.625444, Image = "http://2.bp.blogspot.com/_Wb__Ep4MYbk/S6PyKNn41qI/AAAAAAAAAkI/5Rw5yF1fZfo/s320/P5100013.JPG" },
              new GreenSpace { Id = 6, Name = "Parque da Cidade da Póvoa de Varzim", Description = "Parque urbano público da Póvoa de Varzim em Portugal.", Address = "Amorim", Latitude = 41.398518, Longitude = -8.756199, Image = "https://upload.wikimedia.org/wikipedia/commons/d/d8/Parque_da_Cidade_da_Povoa_Varzim_01.jpg" },
              new GreenSpace { Id = 7, Name = "Parque Municipal da Lavandeira", Description = "Espaço verde localizado na Rua Almeida Garrett, Vila Nova de Gaia", Address = "R. Almeida Garrett, Vila Nova de Gaia", Latitude = 41.121243, Longitude = -8.592701, Image = "http://photos1.blogger.com/blogger/343/948/400/Imagem%20056.jpg" },
              new GreenSpace { Id = 8, Name = "Quinta da Conceicao Tennis Pavilion By Fernando Tavora", Description = "Parque situado junto à via marginal do porto de Leixões, em Matosinhos.", Address = "Leça da Palmeira", Latitude = 41.195937, Longitude = -8.687225, Image = "https://www.matosinhoswbf.pt/thumbs/cmmatosinhos_wbf/uploads/geo_article/image/347/04_1_980_2500.jpg" },
              new GreenSpace { Id = 9, Name = "Quinta do Casalinho", Description = "Propriedade de 23,6 hectares localizada a 16,7 Km da foz do Rio Douro, na freguesia de Olival, a Sul da Quinta do Ferraz.", Address = "Rua Guerra Junqueiro, 2080-321 Almeirim", Latitude = 39.146753, Longitude = -8.684774, Image = "https://www.quintadocasalinhofarto.com/imgs/Casalinho.jpg" },
              new GreenSpace { Id = 10, Name = "Paisagem e Reserva de Mindelo", Description = "Paisagem Protegida Regional do Litoral de Vila do Conde e Reserva Ornitológica de Mindelo", Address = "4485-536 Mindelo", Latitude = 41.321104, Longitude = -8.736334, Image = "http://www2.icnf.pt/portal/ap/amb-reg-loc/resource/img/ppvc-rom/paisag4-cmvc/image" }
          );


        }
    }
}
