﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GreenJourney.Migrations
{
    public partial class CreateGreenJourneyDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Charging Stations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    Address = table.Column<string>(maxLength: 1000, nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    ChargerType = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Charging Stations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Green Spaces",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(maxLength: 600, nullable: false),
                    Address = table.Column<string>(maxLength: 1000, nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Image = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Green Spaces", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Charging Stations",
                columns: new[] { "Id", "Address", "ChargerType", "Latitude", "Longitude", "Name" },
                values: new object[,]
                {
                    { 1, "R. de Anselmo Braancamp 385-245, 4000-487 Porto", "DC & AC", 41.154052, -8.6007020000000001, "Posto Rua de Anselmo Braancamp" },
                    { 2, "Av. da França 315-213, Porto", "DC & AC", 41.161720000000003, -8.6279400000000006, "Posto Av. de França" },
                    { 3, "Praça do Dr. Francisco Sá Carneiro 27, 4350-042 Porto", "DC", 41.161960000000001, -8.5923499999999997, "Posto Praça do Dr. Francisco Sá Carneiro" },
                    { 4, "Av. Sidnio Pais n 415 (Rotunda do Bessa), 4100-012 Porto", "DC", 41.164406999999997, -8.6392120000000006, "Posto Av. Sidónio Pais" },
                    { 5, "Estrada da Circunvalação 10853, 4250-152 Porto", "DC & AC", 41.180250000000001, -8.6349400000000003, "Posto Área de serviço Q8" },
                    { 6, "Jardim do Campo 24 de Agosto, 4300-096 Porto", "DC", 41.149763999999998, -8.5989079999999998, "Posto Campo 24 de Agosto " },
                    { 7, "Estrada da Circunvalação 10853, 4250-152 Porto", "DC", 41.180250000000001, -8.6349400000000003, "Posto Zona industrial " }
                });

            migrationBuilder.InsertData(
                table: "Green Spaces",
                columns: new[] { "Id", "Address", "Description", "Image", "Latitude", "Longitude", "Name" },
                values: new object[,]
                {
                    { 8, "Leça da Palmeira", "Parque situado junto à via marginal do porto de Leixões, em Matosinhos.", "https://www.matosinhoswbf.pt/thumbs/cmmatosinhos_wbf/uploads/geo_article/image/347/04_1_980_2500.jpg", 41.195937000000001, -8.6872249999999998, "Quinta da Conceicao Tennis Pavilion By Fernando Tavora" },
                    { 7, "R. Almeida Garrett, Vila Nova de Gaia", "Espaço verde localizado na Rua Almeida Garrett, Vila Nova de Gaia", "http://photos1.blogger.com/blogger/343/948/400/Imagem%20056.jpg", 41.121243, -8.5927009999999999, "Parque Municipal da Lavandeira" },
                    { 6, "Amorim", "Parque urbano público da Póvoa de Varzim em Portugal.", "https://upload.wikimedia.org/wikipedia/commons/d/d8/Parque_da_Cidade_da_Povoa_Varzim_01.jpg", 41.398518000000003, -8.7561990000000005, "Parque da Cidade da Póvoa de Varzim" },
                    { 5, "Leça do Balio", "Reserva protegida situada no vale do Rio Febros, nas freguesias de Avintes e Vilar de Andorinho.", "http://2.bp.blogspot.com/_Wb__Ep4MYbk/S6PyKNn41qI/AAAAAAAAAkI/5Rw5yF1fZfo/s320/P5100013.JPG", 41.221313000000002, -8.6254439999999999, "Parque Biológico de Gaia" },
                    { 2, "4430-188 Vila Nova de Gaia", "Espaço verde na freguesia de Santa Marinha, em Vila Nova de Gaia, Portugal.", "https://2.bp.blogspot.com/-XVEZBZ4N_30/Wbrn7kw1_EI/AAAAAAABFIc/QowrcKLiomg1Pcyk9f66FlIrXeTn6UJFQCLcBGAs/s1600/jardim%2Bdo%2Bmorro%2B11.jpg", 41.137627000000002, -8.6086840000000002, "Jardim do Morro" },
                    { 3, "4430-188 Vila Nova de Gaia", " Espaço verde localizado na freguesia de Mafamude, em Vila Nova de Gaia, Portugal.", "https://2.bp.blogspot.com/-XVEZBZ4N_30/Wbrn7kw1_EI/AAAAAAABFIc/QowrcKLiomg1Pcyk9f66FlIrXeTn6UJFQCLcBGAs/s1600/jardim%2Bdo%2Bmorro%2B11.jpg", 41.137627000000002, -8.6086840000000002, "Jardim de Soares dos Reis" },
                    { 9, "Rua Guerra Junqueiro, 2080-321 Almeirim", "Propriedade de 23,6 hectares localizada a 16,7 Km da foz do Rio Douro, na freguesia de Olival, a Sul da Quinta do Ferraz.", "https://www.quintadocasalinhofarto.com/imgs/Casalinho.jpg", 39.146752999999997, -8.6847740000000009, "Quinta do Casalinho" },
                    { 1, "Av. Fernão Magalhães 478", "Instituição publica de educação ambiental, dependente da Águas de Gaia, EM, que se dedica a educação ambiental.", "http://www.portoenorte.pt/fotos/oquefazer/1459423184.9344_3751746245a620a1125115.jpg", 41.065229000000002, -8.653473, "Centro de Educação Ambiental das Ribeiras de Gaia" },
                    { 4, "4430-315 Vila Nova de Gaia", "Espaço verde localizado na freguesia de Leça do Balio, concelho de Matosinhos, distrito do Porto, em Portugal, numa zona de fronteira com o concelho da Maia.", "http://www.patrimoniocultural.gov.pt/static/data/cache/57/74/577405c8a719a7677ca8dd8368030b6a.jpg", 41.122906, -8.6120959999999993, "Quinta do Chantre" },
                    { 10, "4485-536 Mindelo", "Paisagem Protegida Regional do Litoral de Vila do Conde e Reserva Ornitológica de Mindelo", "http://www2.icnf.pt/portal/ap/amb-reg-loc/resource/img/ppvc-rom/paisag4-cmvc/image", 41.321103999999998, -8.7363339999999994, "Paisagem e Reserva de Mindelo" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Charging Stations");

            migrationBuilder.DropTable(
                name: "Green Spaces");
        }
    }
}
