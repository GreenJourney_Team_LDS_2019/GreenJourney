﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using GreenJourney.DataAcess;
using GreenJourney.Domain.Services;
using GreenJourney.Domain.Repositories;

namespace GreenJourney
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDbContext<AppDBContext>
                (opt => opt.UseSqlServer(Configuration["Data:GreenJourneyAPIConnection:ConnectionString"]));

            services.AddScoped<IChargingStationRepository, ChargingStationRepository>();
            services.AddScoped<IChargingStationService, ChargingStationService>();
            services.AddScoped<IGreenSpaceRepository, GreenSpaceRepository>();
            services.AddScoped<IGreenSpaceService, GreenSpaceService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseAuthentication();

            app.UseMvc();
        }
    }
}
