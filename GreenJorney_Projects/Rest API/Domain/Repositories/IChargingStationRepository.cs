﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GreenJourney.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace GreenJourney.Domain.Repositories
{
    public interface IChargingStationRepository
    {
        Task<IEnumerable<ChargingStation>> ListAsync();
        Task<ActionResult<ChargingStation>> FindByIdAsync(int id);
        ChargingStation Add(ChargingStation station);
        void Update(ChargingStation station);
        bool Delete(int id);
        ChargingStation FindById(int id);
    }
}
