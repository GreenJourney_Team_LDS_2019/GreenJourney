﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GreenJourney.Domain.Models;
using GreenJourney.DataAcess;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;


namespace GreenJourney.Domain.Repositories
{
    public class GreenSpaceRepository : IGreenSpaceRepository
    {
        private readonly AppDBContext _context;

        public GreenSpaceRepository(AppDBContext context) {
            _context = context;
        }

        public async Task<IEnumerable<GreenSpace>> ListAsync() 
        {
            return await _context.GreenSpaces.ToListAsync();
        }

        public async Task<ActionResult<GreenSpace>> FindByIdAsync(int id) 
        { 
            var space = await _context.GreenSpaces.FindAsync(id);

            if (space == null)
            {
                return null;
            }

            return space;
        }

        public GreenSpace Add(GreenSpace space) 
        {
            _context.AddAsync(space);
            _context.SaveChanges();
            return space;
        }

        public void Update(GreenSpace space) 
        {
            _context.Entry(space).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public bool Delete(int id)
        {
            var space = _context.GreenSpaces.Find(id);

            if (space == null)
            {
                return false;
            }
            else
            {
                _context.GreenSpaces.Remove(space);
                _context.SaveChanges();
                return true;
            }
        }

        public GreenSpace FindById(int id) {
            return _context.GreenSpaces.Find(id);
        }
    }
}
