﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GreenJourney.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace GreenJourney.Domain.Repositories
{
    public interface IGreenSpaceRepository
    {
        Task<IEnumerable<GreenSpace>> ListAsync();
        Task<ActionResult<GreenSpace>> FindByIdAsync(int id);
        GreenSpace Add(GreenSpace space);
        void Update(GreenSpace space);
        bool Delete(int id);
        GreenSpace FindById(int id);
    }
}
