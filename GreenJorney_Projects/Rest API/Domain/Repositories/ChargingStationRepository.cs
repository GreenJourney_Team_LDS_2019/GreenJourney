﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GreenJourney.Domain.Models;
using GreenJourney.DataAcess;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;


namespace GreenJourney.Domain.Repositories
{
    public class ChargingStationRepository : IChargingStationRepository
    {
        private readonly AppDBContext _context;

        public ChargingStationRepository(AppDBContext context) {
            _context = context;
        }

        public async Task<IEnumerable<ChargingStation>> ListAsync() 
        {
            return await _context.ChargingStations.ToListAsync();
        }

        public async Task<ActionResult<ChargingStation>> FindByIdAsync(int id) 
        { 
            var station = await _context.ChargingStations.FindAsync(id);

            if (station == null)
            {
                return null;
            }

            return station;
        }

        public ChargingStation Add(ChargingStation station) 
        {
            _context.AddAsync(station);
            _context.SaveChanges();
            return station;
        }

        public void Update(ChargingStation station) 
        {
            _context.Entry(station).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public bool Delete(int id)
        {
            var station = _context.ChargingStations.Find(id);

            if (station == null)
            {
                return false;
            }
            else
            {
                _context.ChargingStations.Remove(station);
                _context.SaveChanges();
                return true;
            }
        }

        public ChargingStation FindById(int id) {
            return _context.ChargingStations.Find(id);
        }
    }
}
