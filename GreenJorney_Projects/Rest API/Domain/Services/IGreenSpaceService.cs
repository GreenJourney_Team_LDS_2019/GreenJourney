﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GreenJourney.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace GreenJourney.Domain.Services
{
    public interface IGreenSpaceService
    {
        Task<IEnumerable<GreenSpace>> ListAsync();
        Task<ActionResult<GreenSpace>> FindByIdAsync(int id);
        ActionResult<GreenSpace> Save(GreenSpace space);

        ActionResult<GreenSpace> Update(int id, GreenSpace space);
        ActionResult<GreenSpace> Delete(int id);

    }
}
