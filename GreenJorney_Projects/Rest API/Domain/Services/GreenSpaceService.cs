﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GreenJourney.Domain.Models;
using GreenJourney.Domain.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace GreenJourney.Domain.Services
{
    public class GreenSpaceService : IGreenSpaceService
    {
        private readonly IGreenSpaceRepository _greenSpaceRepository;

        public GreenSpaceService(IGreenSpaceRepository greenSpaceRepository) {
            _greenSpaceRepository = greenSpaceRepository;
        }

        public async Task<IEnumerable<GreenSpace>> ListAsync()
        {
            return await _greenSpaceRepository.ListAsync();
        }

        public async Task<ActionResult<GreenSpace>> FindByIdAsync(int id)
        {
            return await _greenSpaceRepository.FindByIdAsync(id);
        }

        public ActionResult<GreenSpace> Save(GreenSpace space) 
        {
            _greenSpaceRepository.Add(space);
            return space;
        }

        public ActionResult<GreenSpace> Update(int id, GreenSpace space)
        {

            if (id != space.Id)
            {
                return null;
            }
            else
            {
                _greenSpaceRepository.Update(space);
                return space;
            }
        }

        public ActionResult<GreenSpace> Delete(int id)
        {
            var removed = _greenSpaceRepository.Delete(id);
            if (removed == false)
            {
                return null;
            }
            else {
                return _greenSpaceRepository.FindById(id);
            }
        }
    }
}
