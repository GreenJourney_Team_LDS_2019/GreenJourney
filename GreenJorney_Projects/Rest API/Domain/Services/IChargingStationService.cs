﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GreenJourney.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace GreenJourney.Domain.Services
{
    public interface IChargingStationService
    {
        Task<IEnumerable<ChargingStation>> ListAsync();
        Task<ActionResult<ChargingStation>> FindByIdAsync(int id);
        ActionResult<ChargingStation> Save(ChargingStation station);

        ActionResult<ChargingStation> Update(int id, ChargingStation station);
        ActionResult<ChargingStation> Delete(int id);

    }
}
