﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GreenJourney.Domain.Models;
using GreenJourney.Domain.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace GreenJourney.Domain.Services
{
    public class ChargingStationService : IChargingStationService
    {
        private readonly IChargingStationRepository _chargingStationRepository;

        public ChargingStationService(IChargingStationRepository chargingStationRepository) {
            _chargingStationRepository = chargingStationRepository;
        }

        public async Task<IEnumerable<ChargingStation>> ListAsync()
        {
            return await _chargingStationRepository.ListAsync();
        }

        public async Task<ActionResult<ChargingStation>> FindByIdAsync(int id)
        {
            return await _chargingStationRepository.FindByIdAsync(id);
        }

        public ActionResult<ChargingStation> Save(ChargingStation station) 
        {
            _chargingStationRepository.Add(station);
            return station;
        }

        public ActionResult<ChargingStation> Update(int id, ChargingStation station)
        {

            if (id != station.Id)
            {
                return null;
            }
            else
            {
                _chargingStationRepository.Update(station);
                return station;
            }
        }

        public ActionResult<ChargingStation> Delete(int id)
        {
            var removed = _chargingStationRepository.Delete(id);
            if (removed == false)
            {
                return null;
            }
            else {
                return _chargingStationRepository.FindById(id);
            }
        }
    }
}
